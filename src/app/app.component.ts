import { Component, Inject, Optional } from '@angular/core';
import { ENVIRONMENT } from './tokens';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(@Optional() @Inject(ENVIRONMENT) public environment: string) {}
  title = 'injectiontokens';
}
