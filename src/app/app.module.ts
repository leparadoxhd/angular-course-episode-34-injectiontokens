import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnvironmentService } from './services/environment.service';
import { ENVIRONMENT } from './tokens';

export function getEnvironment(environmentService: EnvironmentService) {
  return environmentService.calculateEnvironment();
}

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [
    {
      provide: ENVIRONMENT,
      useFactory: getEnvironment,
      deps: [EnvironmentService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
