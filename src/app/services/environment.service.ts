import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentService {
  constructor() {}

  calculateEnvironment() {
    const prod = environment.production;
    if (prod) {
      return 'PROD';
    } else {
      return 'DEV';
    }
  }
}
